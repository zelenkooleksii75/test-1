import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    hotels: [],
    sortedHotels: [],
    sortArray: [],
    selected: "Выберите страну",
    country: [],
    allFilters: {},
  },
  mutations: {
    SET_HOTELS: (state, data) => {
      state.hotels = data;
    },
    SORT_HOTELS: (state, sortedHotels) => {
      state.sortedHotels = sortedHotels;
    },
    SELECT_HOTELS: (state, selected) => {
      state.selected = selected;
    },
    LIST_COUNTRY: (state, uniqueCountry) => {
      state.country = uniqueCountry;
    },
    ADD_FILTER: (state, Obj) => {
      state.allFilters = Obj;
    },
  },
  actions: {
    async addFilter({ commit, dispatch, getters }, filter) {
      let allFilters = await getters.getAllFilters;
      const Obj = Object.assign(allFilters, filter);

      commit("ADD_FILTER", Obj);

      dispatch("allSort");
    },
    async FilterNull({ commit, dispatch}) {
      commit("ADD_FILTER", {});

      dispatch("allSort");
    },
    async allSort({ commit, getters, dispatch }) {
      dispatch("getListCountry");
      let ListHotel = await getters.getListHotel;
      let allFilters = await getters.getAllFilters;

      const filterArrByObj = (arr = [], obj = {}) => {
        const filterEntries = Object.entries(obj);
        return arr.filter((e) =>
          filterEntries.every(([key, value]) => e[key] === value)
        );
      };

      const result = filterArrByObj(ListHotel, allFilters);
      console.log("filterArrByObj", filterArrByObj(ListHotel, allFilters));

      commit("SORT_HOTELS", result);
    },
    async getListCountry({ commit, getters }) {
      const ListHotel = await getters.getListHotel;

      let uniqueCountry = [...new Set(ListHotel.map((item) => item.country))];
      commit("LIST_COUNTRY", uniqueCountry);
    },
    async ListHotel({ commit, dispatch }) {
      const response = await fetch(
        "https://travel-9165f-default-rtdb.europe-west1.firebasedatabase.app/hotels.json"
      );
      const data = await response.json();

      commit("SORT_HOTELS", data);
      commit("SET_HOTELS", data);

      dispatch("getListCountry");

      console.log(data);
    },
  },
  getters: {
    getListHotel: (s) => s.hotels,
    getListSortHotel: (s) => s.sortedHotels,
    getAllFilters: (s) => s.allFilters,
  },
  modules: {},
});
