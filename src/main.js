import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import firebase from "firebase/app";
import "firebase/database";

Vue.config.productionTip = false;

export const db = firebase
  .initializeApp({
    apiKey: "AIzaSyDcjFZsWAdGKYZ3RmkVOAKuaVr8WHeWr7c",
    authDomain: "travel-9165f.firebaseapp.com",
    databaseURL:
      "https://travel-9165f-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "travel-9165f",
    storageBucket: "travel-9165f.appspot.com",
    messagingSenderId: "904580392583",
    appId: "1:904580392583:web:5a90bccb3c155ee33f3a22",
  })
  .database();

export const storage = firebase.database();

new Vue({
  store,
  render: (h) => h(App),
}).$mount("#app");
